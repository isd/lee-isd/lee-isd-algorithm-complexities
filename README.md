# Lee ISD algorithm complexities

final_version.sage: contains functions to compute complexity of Lee ISD algorthms for given set of parameters. 

LeeAsymptotics.nb: Mathematica file to compute asymptotic complexity of Lee ISD algorithms 

Lee-ISD-restricted.nb: Mathematica file that contains asymptotic costs of Lee ISD algorithms that are based on restricted balls idea, viz., BJMM, restricted BJMM, Wagner, restricted Wagner.  
