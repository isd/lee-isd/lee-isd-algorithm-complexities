### These programs are for SAGE

### Auxiliary functions for the computations of the workfactors of the ISD algorithms below.


#  average lee weight of a randomly chosen vector over Z/(p^s)Z
def Leeaverage(p,s):
    if p==2:
        mu= (p^s)/4
    else:
        mu=((p^s)^2-1)/(4*p^s)
    return N(mu)




# number of compositions of a into b parts, each part can take values up to M
def compositionupto(a,b,M):
    if a>b*M:
        comp=0
    elif b>a:
        comp=0
    elif b<=a:
        comp=0
        for j in range(0, min(b,floor((a-b)/M))+1):
            comp=comp+(-1)^j*binomial(b,j)*binomial(a-j*M-1,b-1)
    return comp
   




# number of vectors of length n over Z/qZ of fixed Lee weight w and fixed support size supp  
def countgivensupp(n,supp,w,q):
    M=floor(q/2)
    if q.mod(2)==0:
        if n==0:
            c=0
        elif supp>w:
            c=0
        elif w>supp*M:
            c=0
        elif w==supp*M:
            c=binomial(n,supp)
        elif w<supp*M:
            c=0
            for k in range(0, min(supp, 2*w/q)+1):
                c=c+binomial(n-k,supp-k)*2^(supp-k)*binomial(n,k)*compositionupto(w-k*M,supp-k,M-1)
    if q.mod(2)==1:
        if n==0:
            c=0
        elif supp>w:
            c=0
        elif w>supp*M:
            c=0
        elif w<=supp*M:
            c=binomial(n,supp)*2^supp*compositionupto(w,supp,M)
    return c




 
#  number of vectors of length n over Z/qZ of fixed Lee weight w
def countLee(n,w,q):
    M=floor(q/2)
    if w==0 or n==0:
       c=1
    else:
        c=0
        for s in range(ceil(w/M),min(n,w)+1):
            c=c+countgivensupp(n,s,w,q)
    return c 

# volume of the Lee ball over Z/qZ^n of radius w    
def Leeball(n,w,q):
    c=0
    for i in range(0,w+1):
        c=c+countLee(n,i,q)
    return c    
    
    
##############################################################################
    





# cost of 2-blocks algorithm over Z/(p^s)Z with any m_1,m_2, v_1 and v_2  
# Input: code parameters n, K = k1 + k2 + ... + ks, k1, weight  t, finite ring size p^s
def workfactor_two_blocks_Zpm_v1v2(n,K,k1, t, p, s):
    mincost=1000000000000
    M= floor((p^s)/2)
    gc= (n-k1)^2*(n+1)*(ceil(log(p^s,2))^2+ceil(log(p^s,2)))
    for m1 in range(0, K+1):
        print m1
        m2=K-m1
        for v1 in range(0, min(m1*M, t)+1):
            M1=countLee(m1,v1,p^s)
            B1=Leeball(m1,v1,p^s)
            for v2 in range(0, min(m2*M,t-v1)+1):
                M2= countLee(m2,v2,p^s)
                B2=Leeball(m2,v2,p^s)
                for l in range(0, max(n-K-ceil((t-v1-v2)/M),0)+1):
                    cs= B1*(l+K-k1)*ceil(log(p^s,2))
                    ct= (B2+M2)*(K-k1+l)*ceil(log(p^s,2)) 
                    coll= M1*M2*(p^(-s*(l+K-k1)))*min(Leeaverage(p,s)^(-1)*(t-v1-v2+1),n-K-l)*min(v1+v2,K)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                    ops=gc+cs+ct+coll
                    prob=M1^(-1)*M2^(-1)*countLee(n-K-l,t-v1-v2,p^s)^(-1)*countLee(n,t,p^s)
                    cost=N(log(prob*ops,2))
                    if cost<mincost:
                        bestm1=m1
                        bestm2=m2
                        bestv1=v1
                        bestv2=v2
                        bestl=l
                        mincost=copy(cost)
    cost=copy(mincost)
    l=bestl
    v1=bestv1
    v2=bestv2
    m1=bestm1
    m2=bestm2
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'l=', l,'m1= ',m1, 'm2= ',m2, 'v1= ',v1, 'v2= ',v2, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    



# cost of 2-blocks algorithm over Z/(p^s)Z, where we choose m1=floor(K/2) and v1=v2=v, this is faster and is usually the optimal choice.   
# Input: code parameters n, K = k1 + k2 + ... + ks, k1, weight t, finite ring size p^s
def workfactor_two_blocks_Zpm(n,K,k1, t, p, s):
    mincost=1000000000000
    m1=floor(K/2)
    m2=K-m1
    M= floor((p^s)/2)
    gc= (n-k1)^2*(n+1)*(ceil(log(p^s,2))^2+ceil(log(p^s,2)))
    for v in range(0, min(m1*M, floor(t/2))+1):
        print v
        M1=countLee(m1,v,p^s)
        B1=Leeball(m1,v,p^s)
        M2= countLee(m2,v,p^s)
        B2=Leeball(m2,v,p^s)
        for l in range(0, max(n-K-ceil((t-2*v)/M)+1,0)):
            cs= B1*(l +K-k1)*ceil(log(p^s,2))
            ct= (B2+M2)*(K-k1+l)*ceil(log(p^s,2)) 
            coll= M1*M2*(p^(-s*(l+K-k1)))*min(Leeaverage(p,s)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
            ops=gc+cs+ct+coll
            prob=M1^(-1)*M2^(-1)*countLee(n-K-l,t-2*v,p^s)^(-1)*countLee(n,t,p^s)
            cost=N(log(prob*ops,2))
            if cost<mincost:
                bestv=v
                bestl=l
                mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    

 
############################################################################   
    
   
# Cost of nested loops section C(w1,...ws) of s-blocks algorithm over Z/(p^s)Z
# Input: k=[k1,...,ks+1], w = [w1,...,ws+1], p, s
def sblocks_C(k,w,p,s):
    c = 0
    for j in range(2,s+2):
        f = k[j-1]*k[0]*(ceil(log(p^(j-1),2))^2+ceil(log(p^(j-1),2)))
        pg = 1
        for l in range(2,j):
            f = f + k[j-1]*k[l-1]*(ceil(log(p^(j-1),2))^2+ceil(log(p^(j-1),2)))
            pg = pg*p^(k[l-1]*(1-l))*countLee(k[l-1],w[l-1],p^s)
        c = c + f*pg
    return N(c)
    
    
    
    

# cost of s-blocks algorithm over Z/(p^s)Z    
# Input: code parameters n, k=[k1,k2,...,ks], weight t, size of finite ring p^s
def workfactor_s_blocks_Zpm(n,k,t, p, s):
    mincost=1000000000000
    K = sum(k)     #K= k1 + ... + ks
    M = floor(p^s/2)
    gc = (n-k[0])^2*(n+1)*(ceil(log(p^s,2))^2+ceil(log(p^s,2)))
    T=countLee(n,t,p^s)
    for v in range(0,min(K*M,t)+1):
        print v
        num_iter = countLee(K,v,p^s)^(-1)*countLee(n-K,t-v,p^s)^(-1)*T - 1
        s1 = 0
        for w in Compositions(v,length=s,min_part=0,outer=[M*j for j in k]):
            k.append(n-K)
            ww=list(w)
            ww.append(t-sum(ww))
            s1 = s1 + countLee(k[0],ww[0],p^s)*sblocks_C(k,ww,p,s)
        s2 = s1/(binomial(v+s-1,s-1))
        cost=N(log(num_iter*(gc+s1)+gc+s2,2))
        if cost<mincost:
            bestv=v
            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k[1]))
    print 'Given n= ', n, 'k1=', k[0], 'K=', K,   't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
 
 
 
    
    
# cost of s-blocks second variant algorithm over Z/(p^s)Z  
# Input: code parameters n, k=[k1,k2,...,ks], weight t, size of finite ring p^s
def workfactor_s_blocks_second_variant(n,k,t, p, s):
    mincost=1000000000000
    K = sum(k)     #K= k1 + ... + ks
    M = floor(p^s/2)
    gc = (n-k[0])^2*(n+1)*(ceil(log(p^s,2))^2+ceil(log(p^s,2)))
    T=countLee(n,t,p^s)
    for v1 in range(max(0, t-(n-K)*M),min(k[0]*M,t)+1):
        print v1
        K1= countLee(k[0],v1,p^s)
        for v2 in range(0, min(t-v1,k[1]*M)+1):
            num_iter = K1^(-1)*countLee(k[1],v2,p^s)^(-1)*countLee(n-K,t-v1-v2,p^s)^(-1)*T
            w=[]
            w.append(v1)
            w.append(v2)
            for j in range(0,s-2):
                w.append(0)
            w.append(t-v1-v2)
            k.append(n-K)
            c = K1*sblocks_C(k,w,p,s)
            cost=N(log(num_iter*(gc+c),2))
            if cost<mincost:
                bestv1=v1
                bestv2=v2
                mincost=copy(cost)
    cost=copy(mincost)
    v1=bestv1
    v2=bestv2
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k[1]))
    print 'Given n= ', n, 'k1=', k[0], 'K=', K,   't=',t, 'v1= ',v1, 'v2=', v2, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'    
    
   
    
########################################################################  
    
    
# cost of Wagner's approach
# Input: level a, internal parameter u, l and v, code parameter K,k1, size of finite ring p^s 
def wagner(a, u, K,k1, l, v, p, s):
    b=0
    u.append(K+l-k1)
    L=[countLee( floor((K+l)/(2^a)), floor(v/(2^a)), p^s)]
    beta=[]
    for i in range(1,a+1):
        d=0
        for m in range(1,i):
            d=d+2^(i-m-1)*u[m-1]
        beta.append(u[i-1]+d)
        L.append((L[0]^(2^i))/((p^s)^beta[i-1]))
        b=b+2^(a-i)*(2*L[i-1]*u[i-1]*(K+l)/(2^(a-i+1))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2) +(L[i-1]+1)*log(L[i-1],2))
    return [b, L[a]] 
    
    
    
# cost of the PGE using Wagner's approach  over Z/(p^s)Z  
# Input: code parameterse n, K,k1, weight t, level a, size of finite ring p^s
def workfactor_PGE_wagner(n,K,k1,t,a,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    T=countLee(n,t,p^s)
    for l in range(0,n-K+1):
        print l
        for v in range(max(0, t- (n-K-l)*M), min(t,2^a*(floor((K+l)/2^a))*M)+1): 
            sp= T*countLee(n-K-l,t-v,p^s)^(-1)*countLee(floor((K+l)/(2^a)), floor( v/(2^a)),p^s)^(-(2^a))       
            for u in UnorderedTuples(range(K+l-k1), a-1).list():
                pr=(p^(s*u[0]))^(2^(a-2))
                for i in range(1,a-1):
                    pr=pr*(p^(s*(u[i]-u[i-1])))^(2^(a-i-1))  
                cs= wagner(a,u,K,k1,l,v,p,s)
                it=cs[0]+((n-K-l)^2*(n+1)+ cs[1]*min(v,K+l)*min((n-K-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                cost=N(log(it*sp*pr,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    bestu=u
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'         
        
  
  
###############################################################################

# cost of the representation technique
# Input: level a, internal parameter u, l and v, size of finite ring p^s
def rep_tech(a, u, e, K, k1, l, v, p, s):
    u.append(K+l-k1)
    w=[]
    c=0
    g= sum([floor(e[b-1]/(2^(b-1))) for b in range(1,a+1)])
    L=[countLee(K+l,floor(v/(2^a)+g),p^s)/(p^s)]
    d=g
    for i in range(1,a):
        d=d - floor(e[a-i]/(2^(a-i)))
        w.append(floor(v/(2^(a-i)))+d)
        c=c+2^(a-i)*(2*L[i-1]*u[i-1]*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+ L[i-1]*(2*log(L[i-1],2)-1) + (K+l)*(L[i-1]^2/(p^(s*u[i-1])))*ceil(log(p^s,2)))
        L.append(countLee(K+l, w[i-1],p^s)/(p^(s*u[i-1])))
    return [N(c),N(L[a-1])]
    

 
    

# cost of PGE using representation technique over Z/(p^s)Z  
# Input: code parameters n, K,k1, weight t, level a, internal parameter e, size of finite ring p^s, using for u the lower bound
def workfactor_PGE_rep_tech_low(n,K,k1,t,a,e,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    T=countLee(n,t,p^s)
    for l in range(0,n-K+1):
        for v in range(max(0, t- (n-K-l)*M), min(t,(K+l)*M)+1):
            sp= T*countLee(K+l,v,p^s)^(-1)*countLee(n-K-l,t-v,p^s)^(-1)
            u=[]
            for i in range(1,a):
                ss=0
                for sigma in range(1,min(e[i],K+l)+1):
                    ss=ss+binomial(K+l,sigma)*compositionupto(e[i],sigma, floor(M/2)) 
                ui=floor(log(2*ss,p^s))
                u.append(ui)
            u.append(K+l-k1)
            cs = rep_tech(a,u,e,K,k1,l,v,p,s)
            it=cs[0]+((n-K-l)^2*(n+1)+ 2*cs[1]*(K+l-k1)*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+cs[1]*log(cs[1],2)+cs[1]^2/p^(s*(K+l-k1))*(K+l)*min((n-K-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
            cost=N(log(it*sp,2))
            if cost<mincost:
                bestv=v
                bestl=l
                mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'     
     


# cost of PGE using representation technique over Z/(p^s)Z on one level, where we only go through the minimal and maximal choice of e, which usually is the optimal choice.  
# Input: code parameters n, K,k1, weight t,  internal parameter e, size of finite ring p^s,
def workfactor_PGE_rep_tech_best_e_onelevel(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=1
    T=countLee(n,t,p^s)
    for l in range(0,n-K+1):
        print l
        for v in range(max(0, t- (n-K-l)*M), min(t,(K+l)*M)+1):
            sp= T*countLee(K+l,v,p^s)^(-1)*countLee(n-K-l,t-v,p^s)^(-1)
            for b in [0,(K+l)*M-v]:
                e=[b]
                u=K+l-k1
                cs = rep_tech(a,u,e,K,k1,l,v,p,s)
                it=cs[0]+((n-K-l)^2*(n+1)+ 2*cs[1]*(K+l-k1)*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+cs[1]*log(cs[1],2)+cs[1]^2/p^(s*(K+l-k1))*(K+l)*min((n-K-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                cost=N(log(it*sp,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    beste=e
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'       




# cost of PGE using representation technique over Z/(p^s)Z on two levels, where we only go through the minimal and maximal choice of e, which usually is the optimal choice. using for u the lower bound  
# Input: code parameters n, K,k1, weight t,  internal parameter e, size of finite ring p^s 
def workfactor_PGE_rep_tech_best_e_twolevel_low(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=2
    T=countLee(n,t,p^s)
    for l in range(0,n-K+1):
        print l
        for v in range(max(0, t- (n-K-l)*M), min(t,(K+l)*M)+1):
            sp= T*countLee(K+l,v,p^s)^(-1)*countLee(n-K-l,t-v,p^s)^(-1)
            for c in range(0, (K+l)*M-v+1):
                for b in range(0, (K+l)*M-v/2-c+1):
                    e=[b,c]
                    ss=0
                    for sigma in range(1, min(c,K+l)+1):
                        ss=ss+binomial(K+l,sigma)*compositionupto(c,sigma,floor(M/2))
                    u0= floor(log(2*max(ss,1),p^s))
                    if 0<=u0<=K+l-k1:
                        cs = rep_tech(a,[u0],e,K,k1,l,v,p,s)
                        it=cs[0]+((n-K-l)^2*(n+1)+ 2*cs[1]*(K+l-k1)*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+cs[1]*log(cs[1],2)+cs[1]^2/p^(s*(K+l-k1))*(K+l)*min((n-K-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                        cost=N(log(it*sp,2))
                        if cost<mincost:
                            bestv=v
                            bestl=l
                            bestu0=u0
                            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u0=bestu0
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u0=', u0, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'     
  
  
#######################################################3



# number of compositions up to M
def compositionupto(w,s,M):
    if w>s*M:
        comp=0
    elif s>w:
        comp=0
    elif s<=w:
        comp=0
        for j in range(0, min(s,floor((w-s)/M))+1):
            comp=comp+(-1)^j*binomial(s,j)*binomial(w-j*M-1,s-1)
    return comp

#cost of BJMM in Lee,   
def workfactor_BJMM(n,K,k1,t,p,s):
    mincost=  10000000000
    M=floor((p^s)/2)
    T=countLee(n,t,p^s)
    for l in range(1,n-K+1):
        print 'l=', l
        for v in range(max(1, t- (n-K-l)*M), min(t,(K+l)*M)+1):
            sp= T*countLee(K+l,v,p^s)^(-1)*countLee(n-K-l,t-v,p^s)^(-1)
            for e1 in range(0, M*(K+l)-v+1):
                LL2=countLee(K+l, floor(v/2+e1),p^s)
                s0=0
                for sigma in range(1, min(e1,K+l)+1):
                    s0=s0+binomial(K+l, sigma)*compositionupto(e1,sigma, floor(M/2))
                u0=floor(log(2*max(s0,1),p^s) )
                for e2 in range(0,M*(K+l)-ceil(v/2+e1)+1):
                    LL1=countLee(K+l, floor(v/4+e1/2+e2),p^s)
                    L0=countLee((K+l)/2, floor(v/8+e1/4+e2/2),p^s)
                    s1=0
                    for sigma in range(1, min(e2,K+l)+1):
                        s1=s1+binomial(K+l, sigma)*compositionupto(e2,sigma, floor(M/2))
                    u1=floor(log(2*max(s1,1),p^s) )
                    L2=LL2/p^(s*u1)
                    L1=LL1/p^(s*u0)
                    it=8*L0+4*(2*L0*u0*(K+l)/2*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+(L0+1)*log(L0,2))+2*(2*L1*u1*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+2*L1*log(L1,2)+(K+l)*L1^2/(p^(s*u1))*ceil(log(p^s,2)))+2*L2*(K+l-k1)*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+2*L2*log(L2,2)+L2^2/(p^(s*(K+l-k1)))*(K+l)*min(n-K-l,Leeaverage(p,s)^(-1)*(t-v+1))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                    cost=N(log(it*sp,2))
                    if cost<mincost:
                        bestv=v
                        bestl=l
                        bestu0=u0
                        bestu1=u1
                        beste1=e1
                        beste2=e2
                        mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u0=bestu0
    u1=bestu1
    e1=beste1
    e2=beste2
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u0=', u0, 'u1 =', u1, 'e1=', e1, 'e2=', e2, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
            
            
            
            

#cost of BJMM choosing epsilon=0 in Lee,   
def workfactor_zero_BJMM(n,K,k1,t,p,s):
    mincost=  10000000000
    M=floor((p^s)/2)
    T=countLee(n,t,p^s)
    for l in range(0,n-K+1):
        print l
        for v in range(max(0, t- (n-K-l)*M), min(t,(K+l)*M)+1):
            sp= T*countLee(K+l,v,p^s)^(-1)*countLee(n-K-l,t-v,p^s)^(-1)
            e1=0
            LL2=countLee(K+l, floor(v/2+e1),p^s)
            e2=0
            LL1=countLee(K+l, floor(v/4+e1/2+e2),p^s)
            L0=countLee((K+l)/2, floor(v/8+e1/4+e2/2),p^s)
            for u in UnorderedTuples(range(K+l-k1),2).list():
                L2=LL2/p^(s*u[1])
                L1=LL1/p^(s*u[0])
                it=8*L0+4*(2*L0*u[0]*(K+l)/2*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+(L0+1)*log(L0,2))+2*(2*L1*u[1]*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+2*L1*log(L1,2)+(K+l)*L1^2/(p^(s*u[1]))*ceil(log(p^s,2)))+2*L2*(K+l-k1)*(K+l)*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)+2*L2*log(L2,2)+L2^2/(p^(s*(K+l-k1)))*(K+l)*min(n-K-l,Leeaverage(p,s)^(-1)*(t-v+1))*(ceil(log(p^s,2))+ceil(log(p^s,2))^2)
                cost=N(log(it*sp,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    bestu=u
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e1=', e1, 'e2=', e2, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
            
                    
            
            
            
